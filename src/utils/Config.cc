/*
 * This file is part of Sand6, a C++ continuum-based granular simulator.
 *
 * Copyright 2016 Gilles Daviet <gilles.daviet@inria.fr> (Inria - Université Grenoble Alpes)
 * Copyright 2019 Thibaut Metivet <thibaut.metivet@inria.fr> (Inria - Université Grenoble Alpes)
 *
 * Sand6 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Sand6 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Sand6.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Config.hh"

#include "utils/string.hh"
#include "utils/Log.hh"

#include <cmath>

#include <fstream>
#include <regex>

namespace d6 {

    template< typename dest_t >
        void rescale( dest_t &, Scalar ) { }

    template< >
        void rescale( Scalar & src, Scalar s ) { src *= s ; }
    template< int D >
        void rescale( Eigen::Matrix<Scalar, D, 1> &src, Scalar s ) { src *= s ; }


    Config::Config( OptionsDescription const& optsDesc ) :
        fps(240), substeps(1), nFrames( 1 ),
        box( Vec::Ones() ), res( VecWi::Constant(10) ),
        nSamples(2), randomize( 0 ),
        volMass( 2.5e3 ),
        viscosity( 1.e-3 ),
        gravity( Vec::Zero() ),
        phiMax(0.6), 
        mu(0),
        delta_mu( 0 ), I0( 0.4 ), grainDiameter( 1.e-3 ),
        muRigid( 0.5 ),
        cohesion(0), cohesion_decay(0),
        anisotropy( 0 ), elongation( 1 ), brownian( 0 ),
        initialOri( Vec::Constant(1./3) ),
        enforceMaxFrac( false ), weakStressBC( false ),
        usePG( false ), useInfNorm( D6_DIM == 2 ),
        boundary("cuve"),
        output( true ), exportAllFields( bool(3-D6_DIM) ), dumpPrimalData( 0 ),
        fluidVolMass( 1 ), stokesFactor( 18 ), RZExponent( 0 ),
        windSpeed( Vec::Zero() ),
        newtonian(false), compressibility(0), volumeCorrection( 0),
        m_optsDescription( optsDesc ),
        m_hasTypicalLength(false)
    {
        gravity[WD-1] = -9.81 ;
    }

    void Config::internalize()
    {
        m_units.setTypical( typicalLength(), gravity.norm(), volMass );

#define CONFIG_FIELD( name, type, u ) rescale( name, m_units.fromSI( u ) ) ;
        EXPAND_CONFIG
#undef CONFIG_FIELD

        if( m_hasTypicalLength )
            rescale( m_typicalLength, m_units.fromSI( Units::Length ) );
    }

    bool Config::from_string(const std::string& key, const std::string &val)
    {
        std::istringstream iss ( val ) ;
        return from_string( key, iss ) ;
    }

    bool Config::from_string(const std::string& key, std::istringstream &val )
    {
        bool f = false ;
#define CONFIG_FIELD( name, type, s ) \
        if(key == D6_stringify(name)) \
        { \
            cast(val, name) ; \
            f = true ; \
        }
        EXPAND_CONFIG
#undef CONFIG_FIELD

        if( key == "typicalLength" )
        {
            cast( val, m_typicalLength );
            m_hasTypicalLength = true;
            f = true;
        }

        if( !f ) Log::Warning() << "Warning: '" << key << "' is not a valid config field" << std::endl;
        return f ;
    }

    bool Config::from_file(const std::string &file_name)
    {
        std::ifstream in( file_name ) ;
        if(!in)
            return false ;

        std::string line, key ;

        while( std::getline( in, line ))
        {
            std::istringstream iss ( line ) ;
            if ( (iss >> key) && key[0] != '#' )
            {
                from_string( key, iss ) ;
            }
        }

        return true ;
    }

    bool Config::parseCmdLine( int argc, const char** argv )
    {
        po::command_line_parser parser( argc, argv );
        m_cmdLineParser = std::make_unique<po::command_line_parser>( argc, argv );
        auto cmdLineParser = this->cmdLineParser();
        po::store( cmdLineParser
                .options( m_optsDescription )
                .allow_unregistered()
                .run(), m_vm );
        po::notify( m_vm );

        return true;
    }

    bool Config::parseCfgFile( std::string const& filename )
    {
        std::ifstream ifs( filename );
        if( !ifs )
        {
            Log::Error() << "Error reading file " << filename << std::endl;
            return false;
        }
        // Read file contents
        std::ostringstream oss;
        oss << ifs.rdbuf();
        ifs.close();
        m_cfgFilesStr.push_back( oss.str() );
        // Parse
        std::istringstream cfgstr( oss.str() );
        po::store( parse_config_file( cfgstr, m_optsDescription, true ), m_vm );
        po::notify( m_vm );

        return true;
    }

    ParameterValue Config::option( std::string const& o, std::string const& prefix ) const
    {
        return ::d6::option( m_vm, o, prefix );
    }

    int Config::ioption( std::string const& o, std::string const& prefix ) const
    {
        return ::d6::ioption( m_vm, o, prefix );
    }

    double Config::doption( std::string const& o, std::string const& prefix ) const
    {
        return ::d6::doption( m_vm, o, prefix );
    }

    bool Config::boption( std::string const& o, std::string const& prefix ) const
    {
        return ::d6::boption( m_vm, o, prefix );
    }

    std::string Config::soption( std::string const& o, std::string const& prefix ) const
    {
        return ::d6::soption( m_vm, o, prefix );
    }

    std::vector<double> Config::vdoption( std::string const& o, std::string const& prefix ) const
    {
        return ::d6::vdoption( m_vm, o, prefix );
    }

    std::vector<std::string> Config::vsoption( std::string const& o, std::string const& prefix ) const
    {
        return ::d6::vsoption( m_vm, o, prefix );
    }

    void Config::loadParametersFromCfg()
    {
#define CONFIG_FIELD( name, type, s ) \
        try \
        { \
            name = this->option( D6_stringify(name) ).template as<type>(); \
        } \
        catch( boost::bad_any_cast const& bac ) \
        { \
            Log::Error() << "Error loading option " << D6_stringify(name) << " as " << D6_stringify(type) << std::endl; \
            throw; \
        }
        EXPAND_CONFIG
#undef CONFIG_FIELD
    }

    bool Config::dump(const std::string &file_name, const char *comment ) const
    {
        std::ofstream out( file_name ) ;
        if(!out) {
            Log::Error() << "Could not wrtie config file " << file_name << std::endl ;
            return false ;
        }

        if(comment) {
            out << "# " << comment << std::endl ;
        }

#define CONFIG_FIELD( name, type, s ) \
        out << D6_stringify(name) << "\t" ; d6::dump( out, name ) ; out << "\n" ;
        EXPAND_CONFIG
#undef CONFIG_FIELD

        if( m_hasTypicalLength )
        {
            out << "typicalLength" << "\t"; d6::dump( out, m_typicalLength ); out << "\n";
        }

        return true ;
    }

} //ns hyb2d

