/*
 * This file is part of Sand6, a C++ continuum-based granular simulator.
 *
 * Copyright 2019 Thibaut Metivet <thibaut.metivet@inria.fr> (Inria - Université Grenoble Alpes)
 *
 * Sand6 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Sand6 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Sand6.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef D6_OPTIONS_HH
#define D6_OPTIONS_HH

#include <boost/program_options.hpp>

namespace d6 {

namespace po = boost::program_options;

/*!
 * Utility types for option management
 */
typedef po::options_description OptionsDescription;
typedef po::variables_map ParametersMap;
typedef po::variable_value ParameterValue;

/*!
 * Prefix option name with provided prefix
 */
std::string 
prefixOpt( std::string const& prefix, 
          std::string const& opt,
          std::string const& sep = "." );

/*!
 * Utility functions to retrieve option values from ParametersMap
 */
ParameterValue 
option( ParametersMap const& pm, std::string const& o, std::string const& prefix = "" );
int
ioption( ParametersMap const& pm, std::string const& o, std::string const& prefix = "" );
double
doption( ParametersMap const& pm, std::string const& o, std::string const& prefix = "" );
bool
boption( ParametersMap const& pm, std::string const& o, std::string const& prefix = "" );
std::string
soption( ParametersMap const& pm, std::string const& o, std::string const& prefix = "" );
std::vector<double>
vdoption( ParametersMap const& pm, std::string const& o, std::string const& prefix = "" );
std::vector<std::string>
vsoption( ParametersMap const& pm, std::string const& o, std::string const& prefix = "" );

/*!
 * Generic options description
 */
OptionsDescription
generic_options();

/*!
 * Simulation options description
 */
OptionsDescription
simulation_options();

/*!
 * Generic options for scenarios
 */
OptionsDescription
generic_scenario_options();

/*!
 * Options for the friction solver
 */
OptionsDescription
frictionsolver_options( std::string const& prefix );

/*!
 * Aggregated sand6 options
 */
OptionsDescription
sand6_options( std::string const& prefix );

}

#endif
