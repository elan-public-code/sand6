/*
 * This file is part of Sand6, a C++ continuum-based granular simulator.
 *
 * Copyright 2016 Gilles Daviet <gilles.daviet@inria.fr> (Inria - Université Grenoble Alpes)
 * Copyright 2019 Thibaut Metivet <thibaut.metivet@inria.fr> (Inria - Université Grenoble Alpes)
 *
 * Sand6 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Sand6 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Sand6.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef D6_CONFIG_HH
#define D6_CONFIG_HH

#include "utils/units.hh"
#include "utils/alg.hh"
#include "utils/Options.hh"

#include <string>

namespace d6 {

namespace po = boost::program_options;


#define EXPAND_CONFIG \
	CONFIG_FIELD( fps			, Scalar		,	Units::Frequency	        ) \
	CONFIG_FIELD( substeps			, unsigned		,	Units::None			) \
	CONFIG_FIELD( nFrames			, unsigned		,	Units::None			) \
	\
	CONFIG_FIELD( box			, Vec			,	Units::Length		        ) \
	CONFIG_FIELD( res			, VecWi			,	Units::None			) \
	CONFIG_FIELD( nSamples			, unsigned		,	Units::None			) \
	CONFIG_FIELD( randomize			, Scalar		,	Units::None			) \
	\
	CONFIG_FIELD( volMass			, Scalar		,	Units::VolumicMass	        ) \
	CONFIG_FIELD( viscosity			, Scalar		,	Units::Viscosity	        ) \
	CONFIG_FIELD( gravity			, Vec			,	Units::Acceleration	        ) \
	CONFIG_FIELD( phiMax			, Scalar		,	Units::None			) \
	\
	CONFIG_FIELD( mu			, Scalar		,	Units::None			) \
	\
	CONFIG_FIELD( delta_mu			, Scalar		,	Units::None			) \
	CONFIG_FIELD( I0			, Scalar		,	Units::None			) \
	CONFIG_FIELD( grainDiameter		, Scalar		,	Units::Length		        ) \
	CONFIG_FIELD( muRigid			, Scalar		,	Units::None			) \
	\
	CONFIG_FIELD( cohesion			, Scalar		,	Units::Stress		        ) \
	CONFIG_FIELD( cohesion_decay	        , Scalar		,	Units::None			) \
	\
	CONFIG_FIELD( anisotropy		, Scalar		,	Units::None			) \
	CONFIG_FIELD( elongation		, Scalar		,	Units::None			) \
	CONFIG_FIELD( brownian			, Scalar		,	Units::None			) \
	CONFIG_FIELD( initialOri		, Vec			,	Units::None			) \
	\
	CONFIG_FIELD( enforceMaxFrac	        , bool			,	Units::None		        ) \
	CONFIG_FIELD( weakStressBC		, bool			,	Units::None			) \
	CONFIG_FIELD( usePG			, bool			,	Units::None			) \
	CONFIG_FIELD( useInfNorm		, bool			,	Units::None			) \
	\
	CONFIG_FIELD( scenario			, std::string	        ,	Units::None			) \
	CONFIG_FIELD( boundary			, std::string	        ,      	Units::None			) \
	CONFIG_FIELD( output			, bool			,	Units::None			) \
	CONFIG_FIELD( exportAllFields	        , bool			,	Units::None			) \
	CONFIG_FIELD( dumpPrimalData	        , unsigned		,	Units::None			) \
	\
	CONFIG_FIELD( fluidVolMass		, Scalar		,	Units::VolumicMass	        ) \
	CONFIG_FIELD( stokesFactor		, Scalar		,	Units::None 		        ) \
	CONFIG_FIELD( RZExponent		, Scalar		,	Units::None 		        ) \
	CONFIG_FIELD( windSpeed			, Vec			,	Units::Velocity 	        ) \
	\
	CONFIG_FIELD( newtonian			, bool			,	Units::None			) \
	CONFIG_FIELD( compressibility	        , Scalar		,	Units::None			) \
	CONFIG_FIELD( volumeCorrection	        , Scalar		,	Units::None			) \

struct Config
{
	Config( OptionsDescription const& optsDesc = sand6_options( "" ) ) ;

	bool from_string( const std::string &key, const std::string &value ) ;
	bool from_file(const std::string& file_name) ;

        bool parseCmdLine( int argc, const char** argv );
        bool parseCfgFile( std::string const& filename );

        OptionsDescription const& optionsDescription() const { return m_optsDescription; }

        bool hasCmdLineParser() const { return (bool)m_cmdLineParser; }
        po::command_line_parser const& cmdLineParser() const { return *m_cmdLineParser; }
        std::vector<std::string> const& cfgFilesStr() const { return m_cfgFilesStr; }

        ParametersMap const& vm() const { return m_vm; }
        ParameterValue option( std::string const& o, std::string const& prefix = "" ) const;
        int ioption( std::string const& o, std::string const& prefix = "" ) const;
        double doption( std::string const& o, std::string const& prefix = "" ) const;
        bool boption( std::string const& o, std::string const& prefix = "" ) const;
        std::string soption( std::string const& o, std::string const& prefix = "" ) const;
        std::vector<double> vdoption( std::string const& o, std::string const& prefix = "" ) const;
        std::vector<std::string> vsoption( std::string const& o, std::string const& prefix = "" ) const;

        void loadParametersFromCfg();

	bool dump( const std::string& file_name, const char* comment = nullptr ) const ;

	//! Transform all the parameters from SI to internal units
	void internalize() ;

	const Units& units() const {
		return m_units ;
	}

	Scalar time( unsigned frame_nb ) const {
		return (frame_nb / fps ) ;
	}

	Scalar alpha() const {
		return ( volMass - fluidVolMass ) / fluidVolMass ;
	}

        Scalar typicalLength() const {
#ifdef D6_ADIMENSIONALISE
            if( m_hasTypicalLength )
                return m_typicalLength;
            else
                return (box.array()/res.array().cast<Scalar>()).minCoeff() ;
#else
            return 1;
#endif
        }

	Scalar Stokes() const {
		return  //alpha()/(alpha()+1) *
		        (std::sqrt( gravity.norm() * typicalLength() ) *volMass*grainDiameter*grainDiameter)
		        / (typicalLength() * viscosity * stokesFactor) ;
	}

#define CONFIG_FIELD( name, type, u ) \
	type name ;
	EXPAND_CONFIG
#undef CONFIG_FIELD

private:
	bool from_string( const std::string &key, std::istringstream &value ) ;

        OptionsDescription m_optsDescription;

        std::unique_ptr<po::command_line_parser> m_cmdLineParser;
        std::vector<std::string> m_cfgFilesStr;

        ParametersMap m_vm;

	Units m_units ;
        Scalar m_typicalLength;
        bool m_hasTypicalLength;

} ;

} //ns hyb2d

#endif

