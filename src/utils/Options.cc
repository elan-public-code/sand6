/*
 * This file is part of Sand6, a C++ continuum-based granular simulator.
 *
 * Copyright 2019 Thibaut Metivet <thibaut.metivet@inria.fr> (Inria - Université Grenoble Alpes)
 *
 * Sand6 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Sand6 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Sand6.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "utils/Options.hh"
#include "utils/alg.hh"
#include "utils/string.hh"
#include "utils/Log.hh"

namespace d6 {

std::string 
prefixOpt( std::string const& prefix, 
          std::string const& opt,
          std::string const& sep )
{
    std::string s = prefix;
    if( !s.empty() )
        s += sep;
    return s+opt;
}

ParameterValue 
option( ParametersMap const& pm, std::string const& o, std::string const& prefix )
{
    auto it = pm.find( prefixOpt( prefix, o ) );
    if( it == pm.end() )
    {
        Log::Error() << "Invalid option " << prefixOpt( prefix, o ) << std::endl;
        return ParameterValue();
    }
    return it->second;
}

int
ioption( ParametersMap const& pm, std::string const& o, std::string const& prefix )
{
    int opt;
    try
    {
        opt = option( pm, o, prefix ).as<int>();
    }
    catch( boost::bad_any_cast const& bac ) \
    {
        Log::Error() << "Option " << prefixOpt( prefix, o ) << " is not an int" << std::endl;
        throw;
    }
    return opt;
}

double
doption( ParametersMap const& pm, std::string const& o, std::string const& prefix )
{
    double opt;
    try
    {
        opt = option( pm, o, prefix ).as<double>();
    }
    catch( boost::bad_any_cast const& bac ) \
    {
        Log::Error() << "Option " << prefixOpt( prefix, o ) << " is not a double" << std::endl;
        throw;
    }
    return opt;
}

bool
boption( ParametersMap const& pm, std::string const& o, std::string const& prefix )
{
    bool opt;
    try
    {
        opt = option( pm, o, prefix ).as<bool>();
    }
    catch( boost::bad_any_cast const& bac ) \
    {
        Log::Error() << "Option " << prefixOpt( prefix, o ) << " is not a bool" << std::endl;
        throw;
    }
    return opt;
}

std::string
soption( ParametersMap const& pm, std::string const& o, std::string const& prefix )
{
    std::string opt;
    try
    {
        opt = option( pm, o, prefix ).as<std::string>();
    }
    catch( boost::bad_any_cast const& bac ) \
    {
        Log::Error() << "Option " << prefixOpt( prefix, o ) << " is not a string" << std::endl;
        throw;
    }
    return opt;
}

std::vector<double>
vdoption( ParametersMap const& pm, std::string const& o, std::string const& prefix )
{
    std::vector<double> opt;
    try
    {
        opt = option( pm, o, prefix ).as<std::vector<double>>();
    }
    catch( boost::bad_any_cast const& bac ) \
    {
        Log::Error() << "Option " << prefixOpt( prefix, o ) << " is not a vector<double>" << std::endl;
        throw;
    }
    return opt;
}

std::vector<std::string>
vsoption( ParametersMap const& pm, std::string const& o, std::string const& prefix )
{
    std::vector<std::string> opt;
    try
    {
        opt = option( pm, o, prefix ).as<std::vector<std::string>>();
    }
    catch( boost::bad_any_cast const& bac ) \
    {
        Log::Error() << "Option " << prefixOpt( prefix, o ) << " is not a vector<string>" << std::endl;
        throw;
    }
    return opt;
}

OptionsDescription
generic_options()
{
    OptionsDescription generic( "Generic options" );
    generic.add_options()
        ( "help,h", "Print help" )
        ( "cfg-files", po::value<std::string>(), "Config files" )
        ( "output-dir,o", po::value<std::string>()->default_value( "out" ), "Output directory" )
        ( "verbosity,v", po::value<std::string>()->default_value( "All" ), "Verbosity level" )
        ;

    return generic;
}

OptionsDescription
simulation_options()
{
    OptionsDescription simulation( "Simulation options" );
    simulation.add_options()
        ( "fps", po::value<double>()->default_value( 240. ), "Number of frames per second" )
        ( "substeps", po::value<unsigned int>()->default_value( 1 ), "Number of substeps" )
        ( "nFrames", po::value<unsigned int>()->default_value( 1 ), "Number of frames to compute")

        ( "box", po::value<Vec>()->default_value( Vec::Ones(), "{ 1, 1, 1 }" ), "Dimensions of the simulation box" )
        ( "res", po::value<VecWi>()->default_value( VecWi::Constant( 10 ), "{ 10, 10, 10 }" ), "Discretisation resolution" )

        ( "nSamples", po::value<unsigned int>()->default_value( 2 ), "Number of MPM particles per cell" )
        ( "randomize", po::value<double>()->default_value( 0. ), "Randomise the position of MPM particles" )
        
        ( "volMass", po::value<double>()->default_value( 2.5e3 ), "Material volumetric mass" )
        ( "viscosity", po::value<double>()->default_value( 1e-3 ), "Material viscosity" )
        
        ( "gravity", po::value<Vec>()->default_value( Vec::Zero(), "0, 0, 0" ), "Gravity" )
        
        ( "phiMax", po::value<double>()->default_value( 0.6 ), "Maximal volume fraction" )
        
        ( "mu", po::value<double>()->default_value( 0. ), "Grain-grain friction coefficient" )
        ( "muRigid", po::value<double>()->default_value( 0.5 ), "Solid-grain friction coefficient" )
        ( "delta_mu", po::value<double>()->default_value( 0. ), "mu(I) deltaMu coefficient" )
        ( "I0", po::value<double>()->default_value( 0.4 ), "mu(I) I0 inertia number" )
        ( "grainDiameter", po::value<double>()->default_value( 1e-3 ), "mu(I) typical grain diameter" )

        ( "cohesion", po::value<double>()->default_value( 0. ), "Cohesion stress" )
        ( "cohesion_decay", po::value<double>()->default_value( 0. ), "Cohesion decay" )

        ( "anisotropy", po::value<double>()->default_value( 0. ), "Anisotropy coefficient" )
        ( "elongation", po::value<double>()->default_value( 1. ), "Elongation factor" )
        ( "brownian", po::value<double>()->default_value( 0. ), "Brownian coefficient" )
        ( "initialOri", po::value<Vec>()->default_value( Vec::Constant( 1./3. ), "{ 1/3, 1/3, 1/3 }" ), "MPM particles initial origin" )

        ( "enforceMaxFrac", po::value<bool>()->default_value( false ), "Enforce phi < phiMax" )
        ( "weakStressBC", po::value<bool>()->default_value( false ), "Use weak boundary conditions for stress fields" )
        ( "usePG", po::value<bool>()->default_value( false ), "Use Projected Gradient solver" )
        ( "useInfNorm", po::value<bool>()->default_value( D6_DIM == 2 ), "Use infinity norm" )

        ( "output", po::value<bool>()->default_value( true ), "Export results" )
        ( "exportAllFields", po::value<bool>()->default_value( D6_DIM != 3 ), "Export all fields" )
        ( "dumpPrimalData", po::value<unsigned int>()->default_value( 0 ), "Export primal data" )

        ( "fluidVolMass", po::value<double>()->default_value( 1 ), "Fluid volumetric mass" )
        ( "stokesFactor", po::value<double>()->default_value( 18 ), "Stokes factor" )
        ( "RZExponent", po::value<double>()->default_value( 0 ), "RZExponent" )
        ( "windSpeed", po::value<Vec>()->default_value( Vec::Zero(), "{ 0, 0, 0 }" ), "Wind speed" )
        ( "newtonian", po::value<bool>()->default_value( false ), "Use newtonian fluid" )
        ( "compressibility", po::value<double>()->default_value( 0 ), "Fluid compressibility" )
        ( "volumeCorrection", po::value<double>()->default_value( 0 ), "Volume correction" )
        ;

    return simulation;
}

OptionsDescription
generic_scenario_options( std::string const& prefix )
{
    OptionsDescription scenario( "Scenario options" );
    scenario.add_options()
        ( prefixOpt( prefix, "scenario" ).c_str(), po::value<std::string>()->default_value( "" ), "Scenario configuration" )
        ( prefixOpt( prefix, "boundary" ).c_str(), po::value<std::string>()->default_value( "cuve" ), "Boundary conditions" )
        ;

    return scenario;
}

OptionsDescription
frictionsolver_options( std::string const& prefix )
{
    OptionsDescription frictionsolver( "Simulation options" );
    frictionsolver.add_options()
        ( prefixOpt( prefix, "tolerance" ).c_str(), po::value<double>()->default_value( 1e-6 ), "Friction solver tolerance" )
        ;

    return frictionsolver;
}

OptionsDescription
sand6_options( std::string const& prefix )
{
    auto optsDesc = generic_options()
        .add( simulation_options() )
        .add( generic_scenario_options( prefix ) )
        .add( frictionsolver_options( prefixOpt( prefix, "friction-solver" ) ) )
        ;

    return optsDesc;
}

}
