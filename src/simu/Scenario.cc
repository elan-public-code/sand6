/*
 * This file is part of Sand6, a C++ continuum-based granular simulator.
 *
 * Copyright 2016 Gilles Daviet <gilles.daviet@inria.fr> (Inria - Université Grenoble Alpes)
 *
 * Sand6 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Sand6 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Sand6.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Scenario.hh"

#include "Simu.hh"

#include "utils/Config.hh"
#include "utils/string.hh"

namespace d6 {

struct ScenarioBuilder {

	static ScenarioBuilder& instance() {
		static ScenarioBuilder s_instance ;
		return s_instance ;
	}

	void add( const ScenarioFactory& factory ) {
		m_factories.push_back( &factory );
	}

	std::unique_ptr< Scenario > make( const std::string& str ) const {
		std::unique_ptr< Scenario > ptr ;
		for( unsigned k = m_factories.size(); k ; --k ) {
			ptr = m_factories[k-1]->make( str ) ;
			if( ptr )
				break ;
		}
		assert( ptr ) ;
		return ptr ;
	}

private:
	ScenarioBuilder()
	{
		add(m_defaultFactory) ;
	}

	DefaultScenarioFactory m_defaultFactory ;
	std::vector< const ScenarioFactory* > m_factories ;
};

std::unique_ptr< Scenario > Scenario::parse( const Config& config )
{
#ifdef D6_LEGACY_CFG_FILE
	std::istringstream in( config.scenario ) ;
	std::string line ;
	std::vector< std::string > tok ;

	in >> line ;

	std::unique_ptr< Scenario > pScenar = ScenarioBuilder::instance().make( canonicalize(line) ) ;
	pScenar->m_config = &config ;

	Params params ;
	while( in >> line ) {
		tok.clear() ;
		split( line, ":", tok );
		if( tok.size() == 2 ) {
			params[canonicalize(tok[0])] = canonicalize(tok[1]) ;
		}
	}

#else
	std::unique_ptr< Scenario > pScenar = ScenarioBuilder::instance().make( config.soption( "scenario" ) ) ;
	pScenar->m_config = &config ;
        Params params;
        // Parse cmdline parameters
        if( config.hasCmdLineParser() )
        {
            auto cmdLineParser = config.cmdLineParser();
            po::store( cmdLineParser
                    .options( pScenar->optionsDescription() )
                    .allow_unregistered()
                    .run(), params );
        };
        // Parse cfg file
        for( auto cfgstr: config.cfgFilesStr() )
        {
            std::istringstream iss( cfgstr );
            po::store( parse_config_file( iss, pScenar->optionsDescription(), true ), params );
        }
        po::notify( params );
#endif

	pScenar->init( params ) ;

	return pScenar ;
}

void Scenario::register_factory( const ScenarioFactory& factory )
{
	ScenarioBuilder::instance().add( factory ) ;
}

Scalar Scenario::scalar_param(const Params& params, const std::string& key, Units::Unit unit, Scalar def ) const
{
	Scalar s = def ;
	Params::const_iterator it = params.find(key) ;
	if( it != params.end() ) {
#ifdef D6_LEGACY_CFG_FILE
		cast( it->second, s ) ;
#else
                s = it->second.as<Scalar>();
#endif
	}

	return s * m_config->units().fromSI( unit ) ;

}
std::string Scenario::string_param(const Params &params, const std::string &key, const std::string &def) const
{
	std::string s = def ;
	Params::const_iterator it = params.find(key) ;
	if( it != params.end() ) {
#ifdef D6_LEGACY_CFG_FILE
		s = it->second ;
#else
                s = it->second.as<std::string>();
#endif
	}
	return s ;

}

const Vec& Scenario::box() const {
	return m_config->box ;
}

} //d6
